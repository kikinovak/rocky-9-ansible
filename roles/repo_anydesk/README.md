Ansible Role: `repo_anydesk`
============================

Configure AnyDesk package repository with a priority of 10.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

