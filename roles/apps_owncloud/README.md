Ansible Role: `apps_owncloud`
=============================

Install OwnCloud client.


Role Variables
--------------

- `oc_client_version`: OwnCloud client version

OwnCloud package maintainers can't get their shit together, so this variable
has to be updated manually whenever there's a new release:

- https://download.owncloud.com/desktop/ownCloud/stable/


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

