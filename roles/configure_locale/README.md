Ansible Role: `configure_locale`
================================

Set parameters for:

- System locale

- Console keyboard layout

- X11 keyboard layout


Role Variables
--------------

- `locale`: defaults to `fr_FR.utf8`. Other possible values are `fr_BE.utf8`,
  `de_AT.utf8`, `en_US.utf8`, etc. Use `localectl list-locales` to display
  available locales.

- `console_keymap`: defaults to `ch-fr`. Other possible values are `fr`, `be`,
  `de`, `us`, etc. Use `localectl list-keymaps` to display available console
  keymaps.

- `x11_keymap`: defaults to `ch pc105 fr`. Other possible values are `fr`,
  `be`, `de`, etc. Use `localectl list-x11-keymap-layouts`, `localectl
  list-x11-keymap-models` and `localectl list-x11-keymap-variants` to display
  available X11 keymaps, models and variants.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

