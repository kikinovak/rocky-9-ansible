Ansible Role: `configure_sddm`
==============================

Default to X11 session in SDDM.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

