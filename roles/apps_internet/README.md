Ansible Role: `apps_internet`
=============================

Install Applications > Internet:

- Filezilla

- Google Chrome

- HexChat

- KRDC (KDE Remote Desktop)

- Mozilla Firefox

- Mozilla Thunderbird

- Pidgin

- Transmission


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

