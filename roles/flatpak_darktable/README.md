Ansible Role: `flatpak_darktable`
=================================

Install Darktable Flatpak.


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

