Ansible Role: `repo_eid`
========================

Configure EID Archive package repository with a priority of 10.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

