Ansible Role: `graphical_boot`
==============================

Change default target to `graphical.target`.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

