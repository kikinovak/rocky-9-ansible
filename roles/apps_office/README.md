Ansible Role: `apps_office`
===========================

Install Applications > Office:

- LibreOffice Base

- LibreOffice Calc

- LibreOffice Draw

- LibreOffice Impress

- LibreOffice Math

- LibreOffice Writer


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

