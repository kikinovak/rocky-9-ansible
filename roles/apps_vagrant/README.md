Ansible Role: `apps_vagrant`
============================

Install Vagrant virtualization software.

> Building the `vagrant-libvirt` plugin fails with Vagrant 2.4.2 so we stick
> with version 2.4.1 for the moment.


Requirements
------------

KVM/libvirt (`install_kvm`) must be installed and configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

