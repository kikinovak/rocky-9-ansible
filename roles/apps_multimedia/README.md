Ansible Role: `apps_multimedia`
===============================

Install Applications > Multimedia:

- Audacious

- Audacity

- DVDCSS decoding

- FFmpeg thumbnailer

- Gstreamer plugins

- HandBrake

- Kdenlive

- Mplayer

- Mencoder

- OBS Studio

- VLC


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

