Ansible Role: `install_kvm`
===========================

Install KVM/libvirt hypervisor.


Requirements
------------

Check if the CPU supports hardware virtualization:

```
# grep -E 'svm|vmx' /proc/cpuinfo
flags : ... vmx ...
```

Or:

```
# lscpu | grep Virtual
Virtualization:      VT-x
```


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

