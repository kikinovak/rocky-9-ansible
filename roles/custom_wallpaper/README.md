Ansible Role: `custom_wallpaper`
================================

Replace ugly default wallpaper:

- Plasma desktop

- SDDM login manager


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

