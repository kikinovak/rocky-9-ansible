Ansible Role: `install_kdebase`
===============================

Install a basic KDE desktop environment.


Requirements
------------

Enabled package repositories:

  - Official (`repos_official`)

  - EPEL (`repos_epel`)

  - RPMFusion (`repos_rpmfusion`)


Details
-------

Package lists are obtained using the following command:

```
# dnf group info "KDE (K Desktop Environment)"
```

Needed for X11 session:

  - `plasma-workspace-x11`


Role Variables
--------------

  - `plasma`: packages for a basic KDE desktop

  - `cruft`: useless packages (not to be installed)


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

