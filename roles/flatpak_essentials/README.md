Ansible Role: `flatpak_essentials`
==================================

Install essential Flatpaks:

- CherryTree

- Aisleriot


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

