Ansible Role: `flatpak_protonvpn`
=============================

Install ProtonVPN Flatpak.


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

