Ansible Role: `configure_kickoff`
=================================

Replace default Kickoff menu launchers with a custom selection:

- Dolphin

- Mozilla Firefox

- Mozilla Thunderbird

- LibreOffice Startcenter

Replace default session management buttons with a custom selection:

- Logout

- Reboot

- Shutdown


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

