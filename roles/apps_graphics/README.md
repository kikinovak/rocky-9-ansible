Ansible Role: `apps_graphics`
=============================

Install Applications > Graphics:

- Digikam

- GIMP

- Inkscape

- SANE scanner drivers

- Scribus

- Simple Scan


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

