Ansible Role: `apps_omegat`
===========================

Install OmegaT translation software.


Role Variables
--------------

- `omegat_version`: release

- `omegat_url` : download URL


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

