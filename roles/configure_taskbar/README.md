Ansible Role: `configure_taskbar`
=================================

Replace default taskbar launchers with a custom selection:

- Dolphin

- Mozilla Firefox

- Mozilla Thunderbird

- LibreOffice Startcenter


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

