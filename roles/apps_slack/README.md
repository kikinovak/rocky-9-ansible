Ansible Role: `apps_slack`
==========================

- Setup Slack repository with a priority of 10.

- Install Slack communication software.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

