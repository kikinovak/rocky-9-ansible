Ansible Role: `repos_elrepo`
============================

Configure ELRepo package repositories with a priority of 10:

  - ELRepo (enabled)

  - ELRepo Kernel (disabled)

  - ELRepo Extras (disabled)

  - ELRepo Testing (disabled)


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

