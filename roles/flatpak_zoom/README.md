Ansible Role: `flatpak_zoom`
=============================

Install Zoom Flatpak.


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

