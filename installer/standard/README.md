README
======

Start from a minimal Rocky Linux 9 system.

Setup `root` user and password in Anaconda.

Do **not** setup the initial user.

Run the installer.

Create the initial user manually using `useradd`.

Reboot.
