PC Campanula
============

Start from a minimal Rocky Linux 9 system.

Setup `root` user and password in Anaconda.

Do **not** setup the initial user `luc`.

Language and keyboard settings will also be chosen in Anaconda.


