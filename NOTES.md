NOTES
=====

- Check out the list of weak dependencies when installing KDE and see if
  there's some stuff missing.

- Check out Apps > Utilities > Vym.

- Check out MIME associations.

- OmegaT gets re-downloaded every time the playbook runs.

- HandBrake

- OBS Studio

- NVidia driver
